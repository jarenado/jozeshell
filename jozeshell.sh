#.jozeshell.sh
# lightweight zsh framework. stole a bunch of stuff from oh-my-zsh.

# Add some stuff to fpath
fpath=($ZSH/functions $ZSH/completions $ZSH/options $fpath)

# Load all stock functions (from $fpath files) called below.
autoload -U compaudit compinit && compinit
# autoload -U promptinit && promptinit
# prompt spaceship

zmodload -i zsh/complist


# Style this sucker
source "$ZSH/themes/$ZSH_THEME.zsh-theme"

autoload yo
autoload mcd
autoload weather

# Add alias files here
for config_file ($ZSH/aliases/*.zsh); do
  custom_config_file="${ZSH_CUSTOM}/aliases/${config_file:t}"
  [ -f "${custom_config_file}" ] && config_file=${custom_config_file}
  source $config_file
done


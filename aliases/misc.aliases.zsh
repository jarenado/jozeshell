#For coloring pager. TODO: find a place for this shit
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Aliases I either just added or couldnt think of a file to
# put in.

#taskwarrior
alias t="task"
alias tw="task +work"
alias th="task +home"
alias printmebitch="system-config-printer"

#disk usage
alias dush="du -sh"

# cd aliases
alias ....="cd ../.."
alias vim="nvim"
alias vir="sudo -e"

alias zshconfig="vim ~/.zshrc"
alias vijoze="vim ~/.jozeshell/"
alias loads='cd ~/Downloads'
alias load='loads'
#alias weather='curl http://wttr.in'

# ls options
alias la='ls -A'
alias lr='ls -lt'
alias z='~/.jozeshell/plugins/z/z.sh'

# ls grep options
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias rgf='rg --files | rg'
alias pacscripts="rg -A 10 scripts package.json"

# todo.txt aliases
#alias t='~/bin/todo.sh -d ~/.todo/config'
alias vitodo="vim ~/Dropbox/Ubuntu_todo/inbox.md"
#alias food='t list @groceries'
#alias work='t list @work'
#alias music='t list @music'
#alias awesome='t list @awesome'
#alias next='t list @next'

#alias tn="t add @next"
#alias ta="t add @awesome"
#alias tm="t add @music"
#alias tw="t add @work"
#alias tf="t add @groceries"
#alias ts="t add @spanish"


# Platform specific aliases
case `uname` in
  Darwin)
    alias dir='ls **/* -G'
    alias ls="ls -G"
    alias o="open"
  ;;
  Linux)
    #alias o="nautilus"
    alias o="thunar"
    alias dir='ls **/* --color=auto'
    alias bat='acpi -b'
    alias ls="ls --color=always"
    alias ll='ls -l --color=always| sort -r'
  ;;
esac

alias types='~/bin/types/types.sh'
alias speed='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -'

alias icat="kitty +kitten icat"

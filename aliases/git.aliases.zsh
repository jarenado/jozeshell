alias g="git"
alias gst="git status"
alias push="git push"
alias pull="git pull"
alias co="git checkout"
alias glast="git diff HEAD~1"

alias reload="source ~/.zshrc"

alias thanks="git blame"
alias glnm="git log --pretty=oneline --no-merges"
